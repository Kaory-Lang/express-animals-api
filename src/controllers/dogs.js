import express from "express";
import {
	file_to_string,
	render_html,
	render_css,
	load_script,
	list_items
} from "../utils.js";

const dogsRoute = express.Router();

dogsRoute.get("/", render_html("dogs"));

dogsRoute.get("/styles.css", render_css("dogs"));

dogsRoute.get("/script.js", load_script("dogs"));

dogsRoute.get("/getdogs/:number", list_items("dogs"));

export { dogsRoute };
