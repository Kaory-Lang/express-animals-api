import express from "express";
import {
	file_to_string,
	render_html,
	render_css,
	load_script,
	list_items
} from "../utils.js";

const catsRoute = express.Router();

catsRoute.get("/", render_html("cats"));

catsRoute.get("/styles.css", render_css("cats"));

catsRoute.get("/script.js", load_script("cats"));

catsRoute.get("/getcats/:number", list_items("cats"));

export { catsRoute };
