import express from "express";
import {
	file_to_string,
	render_html,
	render_css,
	load_script,
	list_items
} from "../utils.js";

const ducksRoute = express.Router();

ducksRoute.get("/", render_html("ducks"));

ducksRoute.get("/styles.css", render_css("ducks"));

ducksRoute.get("/script.js", load_script("ducks"));

ducksRoute.get("/getducks/:number", list_items("ducks"));

export { ducksRoute };
