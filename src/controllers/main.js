import express from "express";
import { file_to_string, render_html, render_css } from "../utils.js";

const mainRoute = express.Router();

mainRoute.get("/", render_html("main"));
mainRoute.get("/styles.css", render_css("main"));

export { mainRoute };
