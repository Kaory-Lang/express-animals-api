const render_item = (item) => {
	let container = document.createElement("div");
	let image = document.createElement("div");
	let details = document.createElement("div");

	container.className = "item";
	image.className = "item-image";
	details.className = "item-details";

	image.style.backgroundImage = `url(${item.url})`;
	details.innerHTML = `<p>Name: ${item.name}</p> <p>Age: ${item.age}</p>`;

	container.append(image);
	container.append(details);

	let article = document.getElementById("article");
	article.append(container);


	console.log(item);
};

const render_data = (data) => {
	for (let x = 0; x < data.items.length; x++) {
		render_item(data.items[x]);
	}
};

window.fetch("http://localhost:6969/dogs/getdogs/0")
	.then((response) => response.json())
	.then((data) => render_data(data));
