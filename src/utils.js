import * as fs from "node:fs";

const randomNames = ["Will", "Jil", "Doggie", "Tyson", "Misifu", "Mishu",
					"Firulais", "Doggo", "Bobbie", "Top", "DuckDuck"];

const file_to_string = (view, fileName) => {
	if (!view || typeof(view) !== "string")
		throw new Error("Invalid value for parameter called view");

	if (!fileName || typeof(fileName) !== "string")
		throw new Error("Invalid value for parameter called view");

	const viewPath = `${__dirname}/src/views/${view}/${fileName}`;
	return fs.readFileSync(viewPath).toString();
};

const render_html = (view) => {
	return (req, res) => {
	    res.send(file_to_string(view, "index.html"));
	};
};

const render_css = (view) => {
	return (req, res) => {
		res.append("Content-Type", "text/css");
	    res.send(file_to_string(view, "styles.css"));
	};
};

const load_script = (view) => {
	return (req, res) => {
		res.append("Content-Type", "text/javascript");
	    res.send(file_to_string(view, "script.js"));
	};
};

const list_items = (view) => {
	return (req, res) => {
		let cicles = 0;
		const number = Number(req.params.number);
		const result = {
			items: []
		};

		if (Number.isNaN(number))
			throw new Error("Invalid parameter value, need to be an integer.");

		if (number === 0 || number > 10) {
			cicles = 10;
		} else {
			cicles = number;
		}

		let d1 = 0;
		let d2 = 1;

		for (let x = 1; x <= cicles; x++) {
			let url = "http://";
			const name = get_random_name();
			const age = get_random_age();

			url += `${global.host}:${global.port}/${view}-images/${d1}${d2}.jpg`;
			d2++;

			if (d2 > 9) {
				d1++;
				d2 = 0;
			}

			result.items.push({ url: url, name: name, age: age });
		}

		res.json(result);
	}
};

const random = (min, max) => {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

const get_random_name = () => {
	return randomNames[random(0, randomNames.length - 1)];
};

const get_random_age = () => random(0, 35);

export { 
	file_to_string,
	render_html,
	render_css,
	load_script,
	list_items
};

