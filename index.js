import express from "express";
import * as fs from "node:fs";
import path from "node:path";
import { fileURLToPath } from "url";

import { mainRoute } from "./src/controllers/main.js";
import { catsRoute } from "./src/controllers/cats.js";
import { dogsRoute } from "./src/controllers/dogs.js";
import { ducksRoute } from "./src/controllers/ducks.js";

const app = express();
global.host = "localhost";
global.port = 6969;
global.__dirname = path.dirname(fileURLToPath(import.meta.url));

app.use(express.static(`${__dirname}/src/statics/backgrounds`));
app.use("/cats-images", express.static(`${__dirname}/src/statics/cats`));
app.use("/dogs-images", express.static(`${__dirname}/src/statics/dogs`));
app.use("/ducks-images", express.static(`${__dirname}/src/statics/ducks`));

app.get('/', (req, res) => res.redirect("/main"));
app.use("/main", mainRoute);
app.use("/cats", catsRoute);
app.use("/dogs", dogsRoute);
app.use("/ducks", ducksRoute);

app.listen(global.port, global.host, () => {
	console.log("----------------------------------");
	console.log(`Server started at ${global.host}:${global.port}`);
});
