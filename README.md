# Express animals api

A minimal project that server html pages showing diferent animals
information using API, controller-view paradigm. In order to test the
Express.js library for javascript.

### Use:
- Node v19.0.0
- Npm v9.6.4
- Express.js v5.0.0-beta.1

---

Images are stored in the "src/static" directory categorized by animal.

Html, css files are stored by view in the "src/views" directory.

JavaScript API and the view logic are stored in "src/controllers"
directory.

JSON information for every animal are generated at starting the program.
JSON structure are the next one:

```json
{
	"url": "{image url, saved in the server: string}",
	"name": "{animal name: string}",
	"age": "{age generated randomly: integer}"
}
```

 ### API Endpoints:
 > /cats/getcats/:number [max 10, if zero list all]

 > /dogs/getdogs/:number [max 10, if zero list all]

 > /ducks/getducks/:number [max 10, if zero list all]

### Page views:
> /main

> /cats

> /dogs

> /ducks

### API Response Example:

![API Response Example](./doc/api-response-example.png)

### Screens Views:

![Main](./doc/main-view.png)

![Cats](./doc/cats-view.png)

![Dogs](./doc/dogs-view.png)

![Ducks](./doc/ducks-view.png)
